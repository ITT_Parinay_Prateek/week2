//
//  ViewController.swift
//  LoginDemoApp
//
//  Created by Parinay on 07/02/21.
//

import UIKit

class ViewController: UIViewController {
    
    let data = ["prateek@gmail.com":"prateek","Abc@gmail.com":"Abc","Def@gmail.com":"Def"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var emailTextView: UITextField!
    
    @IBOutlet weak var passwordTextView: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func LoginTapped(_ sender: Any) {
        if let email = emailTextView.text {
            if let pass = passwordTextView.text {
                if data[email] != nil {
                    if data[email] == pass {
                        performSegue(withIdentifier: "showDataSegue", sender: nil)
                    }else {
                        printerr()
                    }
                }else{
                    printerr()
                }
            }else {
                printerr()
            }
        }else {
            printerr()
        }
    }
    
    func printerr() {
        self.errorLabel.text = "Invalid ID or Password"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDataSegue" {
            guard let destinationVC = segue.destination as? DataViewController else {return}
            destinationVC.email = emailTextView.text
        }
    }

}

