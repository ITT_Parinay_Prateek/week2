//
//  data.swift
//  LoginDemoApp
//
//  Created by Raunak Sinha on 07/02/21.
//

import Foundation

struct DataModel {
    var name: string
    var designation: string
    var salary: string
}
