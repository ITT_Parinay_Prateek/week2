//
//  DataViewController.swift
//  LoginDemoApp
//
//  Created by Parinay on 07/02/21.
//

import UIKit

class DataViewController: UIViewController {
    
    struct DataModel {
        var name: String
        var designation: String
        var salary: String
    }
    
    var demo1 = DataModel.init(name: "Prateek", designation: "iOS Dev", salary: "$ 20000")
    var demo2 = DataModel.init(name: "Abc", designation: "Web Dev", salary: "$ 10000")
    var demo3 = DataModel.init(name: "Def", designation: "Devops", salary: "$ 30000")
    
    
    
    var email: String!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var designationLabel: UILabel!
    
    @IBOutlet weak var salaryLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var d: DataModel
        
        if email == "prateek@gmail.com" {
            d=demo1
        }else if email == "Abc@gmail.com"{
            d=demo2
        }else {
            d=demo3
        }
        
        self.nameLabel.text = "NAME :   " + d.name
        self.designationLabel.text = "DESIGNATION :   " + d.designation
        self.salaryLabel.text = "SALARY :   " + d.salary
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
