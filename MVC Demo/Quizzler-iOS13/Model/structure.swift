//
//  structure.swift
//  Quizzler-iOS13
//
//  Created by Parinay on 07/02/21.
//

import Foundation

struct Question {
    let text: String
    let answer : String
    
    init(q: String, a: String) {
         text=q
        answer=a
        
    }
}
